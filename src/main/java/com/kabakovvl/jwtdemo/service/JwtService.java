package com.kabakovvl.jwtdemo.service;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyPair;
import java.util.Date;

/**
 * @author vkabakov
 * 25.05.2022 17:28
 */

@Service
public class JwtService {

  @Value("${secret.string}")
  private String secretString;
  private Key publicKey;
  private boolean isAsync;

  public String createJwt(SignatureAlgorithm algorithm, String id, String issuer, String subject) {
    SecretKey secretKey = null;
    KeyPair keyPair = null;
    isAsync = SignatureAlgorithm.RS256.equals(algorithm);
    Date curTime = new Date(System.currentTimeMillis());

    if (isAsync) {
      keyPair = Keys.keyPairFor(algorithm);
      publicKey = keyPair.getPublic();
    } else {
      secretKey = Keys.hmacShaKeyFor(secretString.getBytes(StandardCharsets.UTF_8));
    }

    JwtBuilder jwtBuilder = Jwts.builder()
      .setId(id)
      .setIssuer(issuer)
      .setIssuedAt(curTime)
      .setSubject(subject)
      .signWith(isAsync ? keyPair.getPrivate() : secretKey, algorithm);

    return jwtBuilder.compact();
  }

  public void checkJwt(String jwt) {
    Key secretKey = isAsync ? publicKey : Keys.hmacShaKeyFor(secretString.getBytes(StandardCharsets.UTF_8));

    Jws<Claims> claimsJws = Jwts.parserBuilder()
      .setSigningKey(secretKey)
      .build()
      .parseClaimsJws(jwt);

    System.out.println("\nJWT Header:\n" + claimsJws.getHeader().toString());
    System.out.println("\nJWT Payload:\n" + claimsJws.getBody().toString());
  }
}
