package com.kabakovvl.jwtdemo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author vkabakov
 * 26.05.2022 17:42
 */

@Service
public class EncryptService {
  @Value("${password}")
  private String password;

  public String argon2Encrypt(String plainText) {
    Argon2PasswordEncoder encoder = new Argon2PasswordEncoder();

    return encoder.encode(plainText);
  }

  public String pbkdf2Encrypt(String plainText) {
    Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();

    return encoder.encode(plainText);
  }

  public String aes256CbcEncrypt(String plainText) {
    String salt = KeyGenerators.string().generateKey();

    return Encryptors.text(password, salt).encrypt(plainText);
  }

  public String aes256GcmEncrypt(String plainText) {
    String salt = KeyGenerators.string().generateKey();

    return Encryptors.delux(password, salt).encrypt(plainText);
  }

  public String sha3Hash(String plainText) throws NoSuchAlgorithmException {
    final MessageDigest digest = MessageDigest.getInstance("SHA3-256");
    final byte[] hashBytes = digest.digest(plainText.getBytes(StandardCharsets.UTF_8));

    return bytesToHex(hashBytes);
  }

  private static String bytesToHex(byte[] hash) {
    StringBuilder hexString = new StringBuilder(2 * hash.length);
    for (byte b : hash) {
      String hex = Integer.toHexString(0xff & b);
      if (hex.length() == 1) {
        hexString.append('0');
      }
      hexString.append(hex);
    }

    return hexString.toString();
  }
}
