package com.kabakovvl.jwtdemo;

import com.kabakovvl.jwtdemo.service.EncryptService;
import com.kabakovvl.jwtdemo.service.JwtService;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

@SpringBootApplication
@RequiredArgsConstructor
public class JwtDemoApplication implements CommandLineRunner {
  private final JwtService jwtService;
  private final EncryptService encryptService;

  public static void main(String[] args) {
    SpringApplication.run(JwtDemoApplication.class, args);
  }

  @Override
  public void run(String... args) throws NoSuchAlgorithmException {
    String ans;

    ans = input("Выберите операцию: \n 1 - Зашифровать строку\n 2 - Сгенерировать JWT");
    switch (ans) {
      case "1" -> {
        ans = input("Введите текст для шифрования:");
        encodeString(ans);
      }
      case "2" -> {
        ans = input("Выберите алгоритм шифрования:\n 1 - HMAC using SHA-256\n 2 - RSASSA-PKCS-v1.5 using SHA-256");
        generateToken(ans);
      }
      default -> System.out.print("Неверный выбор!");
    }
  }

  private static String input(String title) {
    Scanner scanner = new Scanner(System.in);
    System.out.println(title);
    return scanner.next();
  }

  private void encodeString(String input) throws NoSuchAlgorithmException {
    System.out.println("Argon2 encrypted string: " + encryptService.argon2Encrypt(input));
    System.out.println("PBKDF2 encrypted string: " + encryptService.pbkdf2Encrypt(input));
    System.out.println("AES256-CBC encrypted string: " + encryptService.aes256CbcEncrypt(input));
    System.out.println("AES256-GCM encrypted string: " + encryptService.aes256GcmEncrypt(input));
    System.out.println("SHA3-256 hashed string: " + encryptService.sha3Hash(input));
  }

  private void generateToken(String choice) {
    SignatureAlgorithm algorithm = "1".equals(choice) ? SignatureAlgorithm.HS256 : SignatureAlgorithm.RS256;
    String jwt = jwtService.createJwt(algorithm, "111", "ninja.dojo.ru", "something");
    System.out.print(jwt + "\n");

    jwtService.checkJwt(jwt);
  }
}
